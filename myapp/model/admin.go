package model

import "myapp/dataStore/postgres"

type Admin struct {
	FirstName string
	LastName  string
	Email     string
	Password  string
}

const (
	queryInsertAdmin = `INSERT INTO admin(FirstName,LastName,Email,Password) VALUES($1,$2,$3,$4) RETURNING Email;`
	queryGetAdmin    = `SELECT Email,Password FROM admin WHERE Email=$1 and Password=$2;`
)

func (adm *Admin) Create() error {
	err := postgres.Db.QueryRow(queryInsertAdmin, adm.FirstName, adm.LastName, adm.Email, adm.Password).Scan(&adm.Email)
	return err
}

func (adm *Admin) Get() error {
	return postgres.Db.QueryRow(queryGetAdmin, adm.Email, adm.Password).Scan(&adm.Email, &adm.Password)
}

window.onload = function() {
    fetch("/courses")
    .then(res => res.text())
    .then(data => showCourses(data))
}

function addCourse(){
    var data = getFormData()

    //checks whether the given input in cid is string or a number
    //if the given cid is a number only then it returns false where as it returns true
    //we are using not equals since course id can be combination of string and int
    //isNaN- is not a number
    if(!(isNaN(data.cid))){
        alert("Enter valid course ID")
        return
    }else if (data.courseName == " "){
        alert("Course Name cannot be empty.")
        return
    }

    //fetch method is sent to fetch the resources and a promises is return which resolve with the response object
    //Promise then method is used to handle the response but executed only if the promise is resoveld
    //response object type
    fetch("/course",{
        method:"POST",                    
        headers:{"Content-type":"application/json; charset=UTF-8"},
        body:JSON.stringify(data)
    }).then((res) => {
        if(res.ok){
            alert("Success")
            fetch("/course/"+data.cid)
            .then(res => res.text())
            .then(data => showCourse(data))
        }else{
            throw res.statusText
        }
    }).catch((e) => {
        alert(e)
    })

    resetform()
}

function showCourse(data) {
    const course = JSON.parse(data)
    newRow(course)   
}

function showCourses(data){
    const course = JSON.parse(data)

    course.forEach(c => {
        newRow(c)
    }) 
}

function resetform(){
    document.getElementById("cid").value = "";
    document.getElementById("cname").value = "";
}

function newRow(course) {
    var table = document.getElementById("myTable");
    //insertRow method inserts new row in the table 
        var row = table.insertRow(table.length)

        var td = []
        for (i=0;i < table.rows[0].cells.length; i++){
            td[i] = row.insertCell(i);
        }

        td[0].innerHTML = course.cid;
        td[1].innerHTML = course.courseName;
        td[2].innerHTML =  '<input type="button" onclick="deleteCourse(this)" value="delete" id="button-1">';
        td[3].innerHTML =   '<input type="button" onclick="updateCourse(this)" value="edit" id="button-2">';
}

var selectedRow = null;

function updateCourse(r) {
    selectedRow = r.parentElement.parentElement;

    document.getElementById("cid").value = selectedRow.cells[0].innerHTML;
    document.getElementById("cname").value = selectedRow.cells[1].innerHTML;

    var btn = document.getElementById("button-add");
    cid = selectedRow.cells[0].innerHTML;
    
    if (btn) {
        btn.innerHTML = "update";
        btn.setAttribute("onclick","update(cid)")
    }
}

function getFormData(){
    var formData = {
        cid : document.getElementById("cid").value,
        courseName: document.getElementById("cname").value
    }
    return formData
}

function update(cid) {
    var newData = getFormData()

    fetch("/course/"+cid,{
        method:"PUT",
        headers:{"Content-type":"application/json; charset=UTF-8"},
        body:JSON.stringify(newData)
    }).then(res => {
        if (res.ok) {
            selectedRow.cells[0].innerHTML = newData.cid;
            selectedRow.cells[1].innerHTML = newData.courseName;

            var button = document.getElementById("button-add");
            button.innerHTML = "Add";
            button.setAttribute("onclick","addCourse()");
            selectedRow = null;

            resetform()
        }else{
            alert("server: Update request error.")
        }
    })
}

function deleteCourse(r){
    if(confirm("Are you sure you want to DELETE this?")){
        selectedRow = r.parentElement.parentElement;
        cid = selectedRow.cells[0].innerHTML;

        fetch("/course/"+cid,{
            method:"DELETE",
            headers:{"Content-type":"appliation/json; charset=UTF-8"}
        });

        var rowIndex = selectedRow.rowIndex;
        if(rowIndex>0) {
            document.getElementById("myTable").deleteRow(rowIndex);
            selectedRow = null;
        }
    }
}